using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets.Code.entity;
using Assets.Code.globalVar;

public class dragAndFire : MonoBehaviour
{
	    public GameObject entityObject;
        public heroEntity1 hero;
        public Vector2 lastMousePosition;

        public GameObject heroPlayer;

        public global globalVar;

        private bool  isPause= false;
    // Start is called before the first frame update
    void Start()
    {
        // GET HERO ENTITY
            entityObject = GameObject.Find("entity");
            hero = entityObject.GetComponent<heroEntity1>();
            heroPlayer = GameObject.Find("heroStartPos");
    }
    void OnMouseUp(){
    	hero.stopFire();
    }
     void OnMouseDrag() {     	
     	lastMousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
     	// if(heroPlayer.transform.position.x-lastMousePosition.x < -2 && !isPause && globalVar.totalMagaZine > 0){
     	// 	hero.fire(lastMousePosition);
     	// }        
     }

    // Update is called once per frame
    void Update()
    {
        if(globalVar.isPause){
        	isPause = true;
        }else{
        	isPause = false;
        }
    }
}
