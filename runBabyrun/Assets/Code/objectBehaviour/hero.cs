using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using Assets.Code.globalVar;

public class hero : MonoBehaviour
{

        private GameObject bulletObject;
        private GameObject[] bullet;

        private float startPositionX;

        public Animator anim;

        private GameObject slider;

        private Slider Life;
        private float  heroLife= 100;

        public global globalVar;

    // Start is called before the first frame update
    void Start()
    {        
    	anim = gameObject.GetComponent<Animator>();
        Debug.Log(anim);
        slider = GameObject.Find("heroLife");
        Life = slider.GetComponent<Slider>();
        Life.value = heroLife;
    }

    public void fire(){
    	anim.SetBool("shoot",true);
    }

    public void stopFire(){
    	anim.SetBool("shoot",false);
    	anim.SetBool("dontShoot",true);
    }

    public void OnDragDelegate(PointerEventData data)
    {
        Debug.Log("Dragging.");
    }

    // Update is called once per frame
    void Update()
    {
        heroLife = globalVar.heroLife;
        Life.value = heroLife;
    }
    void OnMouseDrag()
    {
        Debug.Log("DRAGGGGGG....");
    }
     void OnCollisionStay2D(Collision2D Obj){
       // if(Obj.gameObject.tag == "enemy"){
       //      heroLife -= 1;
       //      Life.value = heroLife;
       //  }         
    }

    void OnTriggerEnter2D(Collider2D Obj){
        // if(Obj.gameObject.tag == "enemy"){
        //     heroLife -= 1;
        //     Life.value = heroLife;
        // }    
    }

    void FixedUpdate(){
        if(globalVar.heroIsHitting){
            globalVar.heroLife -= .2f;
            Life.value = heroLife;
        }
    }
}
