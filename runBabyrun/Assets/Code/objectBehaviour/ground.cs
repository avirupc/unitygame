using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets.Code.interfaces;
using Assets.Code.state;
using UnityEngine.SceneManagement;
public class ground : MonoBehaviour
{
    private GameObject startPos;
    private GameObject endPoint;
    private Rigidbody2D rigidBody;
    public float startPointX;
    public float startPointY;
    public float poolPosX;
    public bool repos = false;


    void Start()
    {
        // Debug.Log("HUI STRATA");
        startPos = GameObject.Find("start_position");
        endPoint = GameObject.Find("endPoint");
        this.transform.position = new Vector2(startPointX, startPointY);
    }

    public void moveForward(float limit, float speed)
    {
        
        if(limit > 1){
            // Debug.Log(limit);
            rigidBody = gameObject.GetComponent<Rigidbody2D>();
            // rigidBody.AddForce(new Vector2(speed, 0) * limit, ForceMode2D.Impulse);
            rigidBody.velocity = new Vector2(speed, 0) * limit;    
        }else{
            stopGround();
        }
        
    }
    public void stopGround()
    {
        // Debug.Log("LOGA");
        rigidBody = gameObject.GetComponent<Rigidbody2D>();
        rigidBody.velocity = Vector2.zero;
    }


    void Awake()
    {

    }

    // void OnBecameInvisible()
    // {
    //     Debug.Log("LALALALALLA");
    //     gameObject.transform.position = new Vector2(poolPosX, startPointY);
    // }
    void Update()
    {
        if (gameObject.transform.position.x < endPoint.transform.position.x)
        {
            gameObject.transform.position = new Vector2(poolPosX + gameObject.transform.position.x, startPointY);
        }
    }
}