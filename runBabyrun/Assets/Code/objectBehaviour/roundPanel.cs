﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class roundPanel : MonoBehaviour
{
	private GameObject panelObject;
	private panelController controller;

    // Start is called before the first frame update
    void Start()
    {
        panelObject = GameObject.Find("panelController");
        controller = panelObject.GetComponent<panelController>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void OnGUI()
    {
        Event m_Event = Event.current;

        if (m_Event.type == EventType.MouseDown)
        {
        }

        if (m_Event.type == EventType.MouseDrag)
        {
            controller.startDrag();
        }

        if (m_Event.type == EventType.MouseUp)
        {
            controller.endDrag();
        }
    }
}
