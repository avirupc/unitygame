﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Assets.Code.globalVar;

public class enemy : MonoBehaviour
{
	public Vector3 startPos;
	public float heroPositionX;
	private Rigidbody2D rb;
	private Animator anim;
	private float Life;

	private Transform sliderObject;
    private Slider slider;

   	private Transform coinObject;


    private Transform lifeCanvas;
    private lifeBar life;

    private bool dead = false;

    public global globalVar;

    private SpriteRenderer renderer;

    private bool invisible = true;

    private bool heroIsHitting = false;

    public Vector3 heroLifePos;
    private coin Coin;

    public string enemytag;
    // Start is called before the first frame update
    void Start()
    {    	
    	lifeCanvas = gameObject.transform.Find("lifeBar");
    	life = lifeCanvas.gameObject.GetComponent<lifeBar>();

    	renderer = gameObject.GetComponent<SpriteRenderer>();
        rb = gameObject.GetComponent<Rigidbody2D>();
        Life = 100;
        anim = gameObject.GetComponent<Animator>();
        sliderObject = gameObject.transform.Find("lifeBar/Slider");
        slider = sliderObject.gameObject.GetComponent<Slider>();        

        coinObject = gameObject.transform.Find("coin");
        
        go();
        
    }

    public void go(){
    	rb.velocity = new Vector2(heroPositionX,transform.position.y)*.2f;
    }

    public void stop(){
    	rb.velocity = new Vector2(0,0);
    }

    private IEnumerator OnTriggerEnter2D(Collider2D Obj){
       if(Obj.gameObject.name == "bullet(Clone)" && globalVar.firstEnemy == null){
       	Life -= 20;
       	slider.value = Life;
       	anim.SetBool("isHurt",true);
       	life.fadeIn();
       	life.hitting = true;
       	yield return new WaitForSeconds(1);
       	anim.SetBool("isHurt",false);
       }else if(Obj.gameObject.tag == "enemy" && invisible){
       		gameObject.transform.position = new Vector3(Obj.gameObject.transform.position.x+1,Obj.gameObject.transform.position.y,gameObject.transform.position.z);
       }else if(Obj.gameObject.name == "hero(Clone)"){
       		heroIsHitting = true;
       }
    }

    void OnBecameVisible(){
    	// heroLifePos = GameObject.Find("coinEndPosition");
    	// Debug.Log("Hurray");
    	invisible = false;    	
    }

    // private void OnTriggerExit2D(Collider2D bullet)
    // {
    // if(bullet.gameObject.name == "bullet(Clone)")
    // {
    // 	life.hitting = false;
    // }
    // }


   	void die(){
   		dead = true;
   		anim.SetBool("isDie",true);
        rb.velocity = new Vector2(0,0);
        startFadingOut();    
           globalVar.bornEnemy.Remove(enemytag);       
           if(globalVar.bornEnemy.Count == 0){
        	globalVar.instantiateEnemy = true;
        	}
        // CHECKING GLOBAl VAR'S heroIsHitting TRUE OR FALSE IF TRUE MAKE iT FALSE

        if(heroIsHitting){
        	heroIsHitting = false;
        	globalVar.heroIsHitting = false;
        }
    }
  

    IEnumerator fadeOut(){
    	for(float f = 1f; f >= -1f; f-= 0.01f){
    		Color c = renderer.color;
    		c.a = f;
    		renderer.color = c;    		
    		yield return new WaitForSeconds(0.05f);
    	}    	
    }

    public void startFadingOut(){
    	StartCoroutine("fadeOut");
    	
    }

    private void deActivate(){
    	// Debug.Log(gameObject.activeSelf);
    	//if(!coinObject.gameObject.activeSelf){
    		gameObject.SetActive(false);
    		// gameObject.transform.position = startPos;
    		GameObject coinObject =  Resources.Load("attackPrefab/coin") as GameObject ;
        	coinObject.transform.position = gameObject.transform.position;        	
        	GameObject coin = GameObject.Instantiate(coinObject); 
        	Coin = coin.GetComponent<coin>();       
        	Coin.setGlobalVar(globalVar);
            Coin.lifeBarPos = heroLifePos;       	
    	//}
    }



    // Update is called once per frame
    void Update()
    {
    	// CHECKING HERO IS HITTING OR NOT 
    	if(heroIsHitting){
    		globalVar.heroIsHitting = true;
    	}
    	// Debug.Log(dead);
    	if(renderer.color.a < .1){
    		StopCoroutine("fadeOut");
    		deActivate();
    	}

    	if(transform.position.x < heroPositionX+3){
    		rb.velocity = new Vector2(0,0);
    		if(!dead){
    			heroIsHitting = true;
    		}
    	}
        
        if(Life == 0 && !dead || Life < 0 && !dead){
        	die();
        }
    }
}
