﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets.Code.interfaces;
using Assets.Code.globalVar;
using UnityEngine.UI;

namespace Assets.Code.entity

{
	public class heroEntity1 : MonoBehaviour,entityState
{
	
        private GameObject heroObject;
        private GameObject heroAfterLoad;
        private GameObject heroStartPos;
        private GameObject bulletStartPos;
        private hero Hero;

        private GameObject bulletObject;
        private GameObject[] bullet;

        private GameObject enemyObject;
        private GameObject enemy; 

        private GameObject enemyStartPositionObject;

        private bullet[] bulletComponent;
        private enemy enemyComponent;
        private enemy enemyComponent2;

        private float startPositionX;
        private float startPositionY;


        private GameObject bulletQuantityObject;
        private Text bulletQuantityText;

        private GameObject enemyPos;
        private GameObject enemyObject2;
        private GameObject[] enemy2;

        private bool invokeCalled = false;

        // private GameObject reloadButtonObject;
        // private Button reloadButton;

        private GameObject dragObject;
        private dragAndFire dragCS;


        private GameObject incomingSliderObject;
        private Slider incomingSlider;
        private int  hasCame = 0;
        private int enemyRemain;

        private GameObject bulletGraphics;
        private GameObject[] bulletGraphicsAfterLoaded;

        private GameObject bulletGraphicsStartPos;
        private SpriteRenderer sprite;

        private GameObject coinEndPosition;

        public global globalVar;
    // Start is called before the first frame update
    void Start()
    {
    	Debug.Log("GLOBAL VAR ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
            Debug.Log(globalVar.totalMagaZine);
    	// GETTING heroLife OBJECT

    	coinEndPosition = GameObject.Find("coinEndPosition");

    	bulletGraphicsStartPos = GameObject.Find("bulletGraphicsStartPos");
    	// GETTING INCOMING SLIDER

    	incomingSliderObject = GameObject.Find("inComing");
    	incomingSlider = incomingSliderObject.GetComponent<Slider>();    	
    	incomingSlider.maxValue = globalVar.totalIncomingEnemy;
    	enemyRemain = globalVar.totalIncomingEnemy;
    	incomingSlider.value = 0;

    	// GETTING dragAndFire CS AND ASSIGN GLOBAL VAR
    	// dragObject = GameObject.Find("onDrag");
    	// dragCS = dragObject.GetComponent<dragAndFire>();
    	// dragCS.globalVar = globalVar;

    	// RELOAD MENU BUTTON
    	// reloadButtonObject = GameObject.Find("reload");
    	// reloadButton = reloadButtonObject.GetComponent<Button>();
    	// reloadButton.onClick.AddListener(this.reloadFire);

    	enemyStartPositionObject = GameObject.Find("enemy");

    	heroStartPos = GameObject.Find("heroStartPos");
    	bulletStartPos = GameObject.Find("bulletStartPos");
       // heroObject = Resources.Load("attackPrefab/hero") as GameObject;
            //heroObject.transform.position = heroStartPos.transform.position;
            //heroAfterLoad = GameObject.Instantiate(heroObject) as GameObject;
            heroAfterLoad = GameObject.Find("hero") as GameObject;
            Hero = heroAfterLoad.GetComponent<hero>();
        Hero.globalVar = globalVar;
        // Hero = heroObject.GetComponent<hero>();

        startPositionX = bulletStartPos.transform.position.x;
        startPositionY = bulletStartPos.transform.position.y;
        bullet = new GameObject[globalVar.bulletlimit];
        bulletComponent = new bullet[globalVar.bulletlimit];       
    	
    	bulletGraphicsAfterLoaded = new GameObject[globalVar.bulletlimit];

        for (int i = 0; i < globalVar.bulletlimit; i++){

        // INSTANTIATE BULLET GRAPHICS //
        bulletGraphics = Resources.Load("attackPrefab/bulletIndication") as GameObject;

        sprite = bulletGraphics.GetComponent<SpriteRenderer>();
        float bulletWidth = sprite.bounds.size.x+.5f;

        bulletGraphics.transform.position = new Vector2(bulletGraphicsStartPos.transform.position.x+(bulletWidth*i),bulletGraphicsStartPos.transform.position.y);
        bulletGraphicsAfterLoaded[i] = 	 GameObject.Instantiate(bulletGraphics) as GameObject;


                // INSTANTIATE BULLET
                bulletObject = Resources.Load("attackPrefab/bullet") as GameObject;
        bullet[i] = GameObject.Instantiate(bulletObject) as GameObject;
        bullet[i].transform.position = new Vector2(startPositionX,startPositionY);

        bulletComponent[i] = bullet[i].GetComponent<bullet>();
        bulletComponent[i].globalVar = globalVar;
        }
        bulletQuantityObject = GameObject.Find("bulletQuantityIndication");
        bulletQuantityText = bulletQuantityObject.GetComponent<Text>();

        bulletQuantityText.text = globalVar.totalMagaZine.ToString();
    }

    public void countEnemyandSetSliderValue(int enemyCount){
    	hasCame += enemyCount;
    	enemyRemain -= enemyCount;
    	if(enemyRemain == 0){
    		globalVar.isWon = true;
    	}
    	globalVar.enemyRemain -= enemyCount;
    	incomingSlider.value = hasCame;
    }

    private int createRandomNumber(int min, int max){
    	int randomNumberEnemy = 0;
    	randomNumberEnemy = Random.Range(min,max);
    	Debug.Log(enemyRemain);
            Debug.Log(hasCame + randomNumberEnemy);
    	if(hasCame + randomNumberEnemy > enemyRemain){
    		Debug.Log("enemyRemain");
    		randomNumberEnemy = enemyRemain;
    	}else if(hasCame + randomNumberEnemy == globalVar.totalIncomingEnemy)
    	{
    		Debug.Log("DEBUG GAYA");
    	}
    	return randomNumberEnemy;
    }

    public void instantiateEnemy(){    	
    	globalVar.instantiateEnemy = false;
    	if(globalVar.activeRound == "round1"){
	    	int randomNumberEnemy = createRandomNumber(2,5);
                Debug.Log("HAI HAI");
                Debug.Log(randomNumberEnemy);
	    	countEnemyandSetSliderValue(randomNumberEnemy);                
                enemy2 = new GameObject[randomNumberEnemy];
	    	// globalVar.bornEnemy = new string[randomNumberEnemy];

	    	for(int x = 0 ; x < randomNumberEnemy ; x++){    		
	    		enemyPos = GameObject.Find("enemyPos");
		    	enemyObject2 = Resources.Load("attackPrefab/enemy_3") as GameObject;
		    	enemy2[x] = GameObject.Instantiate(enemyObject2);                
		    	enemyComponent2 = enemy2[x].GetComponent<enemy>();
		    	enemyComponent2.enemytag = "enemy"+x;
		    	enemyComponent2.heroLifePos = coinEndPosition.transform.position;
		        globalVar.bornEnemy.Add(enemyComponent2.enemytag);       
		        enemyComponent2.heroPositionX = heroAfterLoad.transform.position.x;
		        enemy2[x].transform.position = enemyPos.transform.position;
		        enemyComponent2.globalVar = globalVar;
                    Debug.Log("HAI HAI Again");
                    Debug.Log(enemyComponent2.globalVar);
                }
    	}else if(globalVar.activeRound == "round2") {

    		int randomNumberEnemy = createRandomNumber(3,6);
    		countEnemyandSetSliderValue(randomNumberEnemy);

	    	enemy2 = new GameObject[randomNumberEnemy];
	    	// globalVar.bornEnemy = new string[randomNumberEnemy];

	    	for(int x = 0 ; x < randomNumberEnemy ; x++){    		
	    		enemyPos = GameObject.Find("enemyPos");
		    	enemyObject2 = Resources.Load("attackPrefab/round_3") as GameObject;
		    	enemy2[x] = GameObject.Instantiate(enemyObject2);

		    	enemyComponent2 = enemy2[x].GetComponent<enemy>();
		    	enemyComponent2.enemytag = "enemy"+x;
		        globalVar.bornEnemy.Add(enemyComponent2.enemytag);       
		        enemyComponent2.heroPositionX = heroAfterLoad.transform.position.x;
		        enemy2[x].transform.position = enemyPos.transform.position;
		        enemyComponent2.globalVar = globalVar;
	    	}
    	}
    	
    	
    }

    public void fire(Vector2 dest){
    	if(!globalVar.isReloading){
    		Hero.fire();
    		// Debug.Log(Hero.anim);
    		invokeCalled = false; 
    		bulletGraphicsAfterLoaded[globalVar.currentIndex].SetActive(false);   		
    	bullet[globalVar.currentIndex].SetActive(true);
    	float directionX = dest.x - bullet[1].transform.position.x;
    	float directionY = dest.y - bullet[1].transform.position.y;    	
    	Vector2 direction = new Vector2(directionX,directionY);    	
    		bulletComponent[globalVar.currentIndex].goFlag = true;
    		bulletComponent[globalVar.currentIndex].destination = direction;
    		bulletComponent[globalVar.currentIndex].limit = dest.x;
    		bulletComponent[globalVar.currentIndex].go(); 
    		bulletComponent[globalVar.currentIndex].isInstantiated = true;  
    	}    	
    }

    public void stopFire(){
    	Hero.stopFire();
    }

    void reload(){
    	globalVar.reload();
    	bulletGraphicsAfterLoaded[globalVar.currentBulletIndex-1].SetActive(true);
    	invokeCalled = true;
	}

	// void reloadFire(){
	// 	InvokeRepeating("reload",0f,.5f);
	// }

    // Update is called once per frame
    void Update()
    {
    	bulletGraphicsStartPos = GameObject.Find("bulletGraphicsStartPos");
    	bulletQuantityText.text = globalVar.totalMagaZine.ToString();
    	if(globalVar.isReloading && globalVar.totalMagaZine > 0){
    		stopFire();
    		if(!invokeCalled)    		
    		InvokeRepeating("reload",0f,.2f);
    	}
        if(globalVar.currentBulletIndex == globalVar.bulletlimit){
        	globalVar.isReloading = false;
        	CancelInvoke("reload");
        }

        if(globalVar.instantiateEnemy){
        	instantiateEnemy();
        }
        if(globalVar.isFiring){
        	fire(globalVar.lastMousePosition);
        }else{
        	stopFire();
        }
    }

}
}