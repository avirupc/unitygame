using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Assets.Code.interfaces;

namespace Assets.Code.entity
{
    public class groundEntity:entityState
    {

        private GameObject[] grounds;
        private GameObject singleGround;
        private SpriteRenderer sprite;
        private ground groundCS;
        private float groundWidth;
        private float startpointX;
        public float startpointY;
        public float poolPosX;

        private GameObject[] grounds2;
        private GameObject singleGround2;
        private SpriteRenderer sprite2;
        private float groundWidth2;


        private GameObject[] grounds3;
        private GameObject singleGround3;
        private SpriteRenderer sprite3;
        private float groundWidth3;
        private ground groundCS4;


        private GameObject[] grounds4;
        private GameObject singleGround4;
        private SpriteRenderer sprite4;
        private float groundWidth4;
        private ground groundCS3;



        private GameObject[] grounds5;
        private GameObject singleGround5;
        private SpriteRenderer sprite5;
        private ground groundCS5;


        public groundEntity()
        {

            grounds = new GameObject[5];
            grounds2 = new GameObject[5];
            grounds3 = new GameObject[4];
            grounds4 = new GameObject[5];
            grounds5 = new GameObject[2];



            for (int i = 0; i < 5; i++)
            {
                // initiating front ground
                singleGround = Resources.Load("groundPrefab/normal_ground") as GameObject;
                sprite = singleGround.GetComponent<SpriteRenderer>();
                groundWidth = sprite.bounds.size.x;
                groundCS = singleGround.GetComponent<ground>();

                Debug.Log(groundWidth);
                if (i != 0)
                {
                    startpointX = GameObject.Find("start_position").transform.position.x + groundWidth * i;
                }
                else
                {
                    startpointX = GameObject.Find("start_position").transform.position.x;
                }
                startpointY = GameObject.Find("start_position").transform.position.y;
                poolPosX = groundWidth * 5;
                groundCS.startPointX = startpointX;
                groundCS.startPointY = startpointY;
                groundCS.poolPosX = poolPosX;
                grounds[i] = GameObject.Instantiate(singleGround);
            }

            for (int i = 0; i < 5; i++)
            {
                // instantiate hill


                singleGround4 = Resources.Load("groundPrefab/g4") as GameObject;
                sprite4 = singleGround4.GetComponent<SpriteRenderer>();
                float groundWidth4 = sprite4.bounds.size.x;
                groundCS4 = singleGround4.GetComponent<ground>();
                float startpointX2;

                Debug.Log(groundWidth);
                if (i != 0)
                {
                    startpointX2 = GameObject.Find("start_position").transform.position.x + groundWidth4 * i;
                }
                else
                {
                    startpointX2 = GameObject.Find("start_position").transform.position.x;
                }
                float startpointY2 = GameObject.Find("start_position").transform.position.y + 2;
                float poolPosX2 = groundWidth4 * 5;
                groundCS4.startPointX = startpointX2;
                groundCS4.startPointY = startpointY2;
                groundCS4.poolPosX = poolPosX2;
                grounds4[i] = GameObject.Instantiate(singleGround4);
            }

            for (int i = 0; i < 5; i++)
            {
                //Instantiate background
                singleGround2 = Resources.Load("groundPrefab/g2") as GameObject;
                sprite2 = singleGround2.GetComponent<SpriteRenderer>();
                groundWidth2 = sprite2.bounds.size.x;
                groundCS = singleGround2.GetComponent<ground>();

                Debug.Log(groundCS);
                if (i != 0)
                {
                    startpointX = GameObject.Find("start_position").transform.position.x + groundWidth2 * i;
                }
                else
                {
                    startpointX = GameObject.Find("start_position").transform.position.x;
                }
                startpointY = GameObject.Find("start_position").transform.position.y + 1f;
                poolPosX = groundWidth2 * 5;
                groundCS.startPointX = startpointX;
                groundCS.startPointY = startpointY;
                groundCS.poolPosX = poolPosX;
                grounds2[i] = GameObject.Instantiate(singleGround2);
            }

            for (int i = 0; i < 4; i++)
            {
                // instantiate house
                singleGround3 = Resources.Load("groundPrefab/home") as GameObject;
                sprite3 = singleGround3.GetComponent<SpriteRenderer>();
                groundWidth3 = sprite3.bounds.size.x;
                groundCS = singleGround3.GetComponent<ground>();

                Debug.Log(groundCS);
                if (i != 0)
                {
                    startpointX = GameObject.Find("start_position").transform.position.x + groundWidth2 * i + Random.Range(5, 15);
                }
                else
                {
                    startpointX = GameObject.Find("start_position").transform.position.x;
                }

                startpointY = GameObject.Find("start_position").transform.position.y + Random.Range(1.5f, 2.5f);
                poolPosX = groundWidth2 * 6;
                groundCS.startPointX = startpointX;
                groundCS.startPointY = startpointY;
                groundCS.poolPosX = poolPosX;
                grounds3[i] = GameObject.Instantiate(singleGround3);
            }

            for (int i = 0; i < 2; i++)
            {
                // initiating front JHAU
                singleGround5 = Resources.Load("groundPrefab/jh1") as GameObject;
                float randomScale = Random.Range(.5f, 2f);
                singleGround5.transform.localScale = new Vector2(randomScale, randomScale);
                sprite5 = singleGround5.GetComponent<SpriteRenderer>();
                groundCS5 = singleGround5.GetComponent<ground>();

                if (i != 0)
                {
                    startpointX = GameObject.Find("start_position").transform.position.x + groundWidth2 * i;
                }
                else
                {
                    startpointX = GameObject.Find("start_position").transform.position.x;
                }
                startpointY = GameObject.Find("start_position").transform.position.y + Random.Range(-1f, 0f);
                poolPosX = (groundWidth2 + Random.Range(2f, 15f)) * 5;
                groundCS5.startPointX = startpointX;
                groundCS5.startPointY = startpointY;
                groundCS5.poolPosX = poolPosX;
                grounds5[i] = GameObject.Instantiate(singleGround5);
            }



        }



        public void addForce(float speedlevel)
        {
            for (int i = 0; i < this.grounds.Length; i++)
            {
                groundCS = grounds[i].GetComponent<ground>();
                groundCS.moveForward(speedlevel, -.1f);
            }
            for (int i = 0; i < this.grounds2.Length; i++)
            {
                groundCS = grounds2[i].GetComponent<ground>();
                groundCS.moveForward(speedlevel / 2, -.1f);
            }
            for (int i = 0; i < this.grounds3.Length; i++)
            {
                groundCS = grounds3[i].GetComponent<ground>();
                groundCS.moveForward(speedlevel / 2, -.1f);
            }
            for (int i = 0; i < this.grounds4.Length; i++)
            {
                groundCS = grounds4[i].GetComponent<ground>();
                groundCS.moveForward(speedlevel / 5, -.1f);
            }
            for (int i = 0; i < this.grounds5.Length; i++)
            {
                groundCS = grounds5[i].GetComponent<ground>();
                groundCS.moveForward(speedlevel * 2, -.1f);
            }
        }
        public void stopGround()
        {
            for (int i = 0; i < this.grounds.Length; i++)
            {
                groundCS = grounds[i].GetComponent<ground>();
                groundCS.stopGround();
            }
            for (int i = 0; i < this.grounds2.Length; i++)
            {
                groundCS = grounds2[i].GetComponent<ground>();
                groundCS.stopGround();
            }

            for (int i = 0; i < this.grounds3.Length; i++)
            {
                groundCS = grounds3[i].GetComponent<ground>();
                groundCS.stopGround();
            }
        }
        public void fire(Vector2 dest){

        }
         public void stopFire(){
         }
    }
}