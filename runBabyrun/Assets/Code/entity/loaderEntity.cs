﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets.Code.interfaces;
using Assets.Code.globalVar;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Assets.Code.state;

namespace Assets.Code.entity

{
	public class loaderEntity : MonoBehaviour,entityState
{
	private GameObject barObject;
    private Slider bar;
    public string sceneName;

    public global globalVar;
    public stateManager manager;
    // Start is called before the first frame update
    void Start()
    {
        barObject = GameObject.Find("bar");
        bar = barObject.GetComponent<Slider>();  
    }

     public void LoadButton()
    	{
            Debug.Log("STAT");
            StopCoroutine("LoadScene");
            //Start loading the Scene asynchronously and output the progress bar
            StartCoroutine("LoadScene");
    	}

    public	IEnumerator LoadScene(){            
        //yield return new WaitForSeconds(1);
            Debug.Log("STAT AGAIN");
            yield return null;            
            AsyncOperation asyncOperation = SceneManager.LoadSceneAsync(sceneName);
        asyncOperation.allowSceneActivation = false;
            Debug.Log("FALSE AGAIN");
            Debug.Log(asyncOperation.isDone);
            while (!asyncOperation.isDone)
        {
                Debug.Log("NOT DONE");
            //Output the current progress
            bar.value = asyncOperation.progress;
            // Debug.Log(globalVar.nextState);
            // Check if the load has finished
            if (asyncOperation.progress >= 0.9f)
            {            
            if (sceneName == "attackState")
                    {
         		asyncOperation.allowSceneActivation = true; 
         		manager.switchState(new attackState(manager,globalVar));  
         		       		
         	}else if(sceneName == "sceneSelectionState"){
         		asyncOperation.allowSceneActivation = true; 
         		manager.switchState(new sceneSelectionState(manager,globalVar));         		
         	}else if(sceneName == "menuState") {           
         	  asyncOperation.allowSceneActivation = true;
              manager.switchState(new menuState(manager,globalVar));              
            }
                    else if (sceneName == "credits")
                    {
                        asyncOperation.allowSceneActivation = true;
                        manager.switchState(new creditState(manager, globalVar));
                    }
                    yield return null; 
        }
    	}          
        }
    	public void stopFire(){
    	}
    	public void fire(Vector2 vec){
    	}


    // Update is called once per frame
    void Update()
    {
        
    }
}
}
