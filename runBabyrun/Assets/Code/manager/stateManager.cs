using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets.Code.interfaces;
using Assets.Code.state;
using UnityEngine.SceneManagement;
using Assets.Code.entity;
using Assets.Code.globalVar;
public class stateManager : MonoBehaviour
{
    public global globalVariable;
    private iState activeState;
    private static stateManager instanceRef;
    // Start is called before the first frame update
    public entityState entity;
    public float speedlevel = 0;
    public bool increasing = false;
    public Vector2 lastMousePosition;
    public Vector2 lastMousePointer;

    // Loader
    private Transform loader;

    void OnEnable()
    {        
        // Debug.Log("Dima pur");
        SceneManager.sceneLoaded += OnSceneLoaded;

    }

    void OnDisable(){
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }

    public iState getActiveState()
    {
        return activeState;
    }
    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        if (activeState == null)
        {
            activeState = new loaderState(this, globalVariable, "menuState");
        }
        //loader.gameObject.SetActive(false);
        Debug.Log("THIS is ACTIVE STATE ===========================================================================================================================");
        Debug.Log(scene.name);        
        activeState.getData();

        if (scene.name == "menuState")
        {
            Screen.orientation = ScreenOrientation.Portrait;
        }
        else
        {
            Screen.orientation = ScreenOrientation.Landscape;
        }
    }
    void Start()
    {
        if(globalVariable == null){
            globalVariable = new global();
        } if (activeState == null)
        {
            activeState = new loaderState(this, globalVariable, "menuState");            
        }
    }
    void Awake()
    {
        //Getting loader
        loader = gameObject.transform.Find("Loader");
        //loader.gameObject.SetActive(true);
        if (instanceRef == null)
        {
            instanceRef = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            DestroyImmediate(this.gameObject);
        }
        if(globalVariable == null){
            globalVariable = new global();
        }
    }
    public void switchState(iState newState)
    {
        // loader.gameObject.SetActive(true);
        activeState = newState;
    }
    void OnMouseDown() {
        // lastMousePosition = Input.mousePosition;
        globalVariable.lastMousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        // Debug.Log(Input.mousePosition);
        lastMousePointer = Input.mousePosition;
        // if(this.activeState.getStateName() == "swampAttackState"){
        //         if(entity == null){
        //             entity = activeState.GetEntity();    
        //         }
        //         lastMousePosition =Camera.main.ScreenToWorldPoint(Input.mousePosition);                
        //         entity.fire(lastMousePosition);
        //     }
    }
    void OnMouseUp(){
         globalVariable.isFiring = false;         
    }

        void OnMouseDrag() {        
            globalVariable.lastMousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            if(!globalVariable.isPause){
                globalVariable.isFiring = true;             
            }
        }
    
    // Update is called once per frame
    void Update()
    {
        


        if (activeState != null)
        {
            activeState.stateUpdate();
        }
        if (Input.GetMouseButtonDown(0))
        {

            // Debug.Log(this.activeState.getStateName());
            if (this.activeState.getStateName() == "playState")
            {
                increasing = true;
                // entity = activeState.GetEntity();
                // activeState = new moveState(this, entity);
            }
        }
        if (Input.GetMouseButtonUp(0))
        {
            // increasing = false;
            // Debug.Log("HOLY SHIT");
            if (this.activeState.getStateName() == "playState")
            {
                // entity = activeState.GetEntity();
                // activeState = new playState(this, entity);
                increasing = false;
            }
        }
    }
}
