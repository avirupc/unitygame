﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets.Code.interfaces;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Assets.Code.entity;
using Assets.Code.globalVar;
namespace Assets.Code.state
{
    public class creditState : iState
    {
        private stateManager manager;
        public string stateName;
        public groundEntity entity;

        public Button btn;
        public GameObject btnObject;

        public global globalVar;

        // Start is called before the first frame update
        public creditState(stateManager managerRef, global globalRef)
        {
            manager = managerRef;
            globalVar = globalRef;
            stateName = "creditState";
        }
        public void getData()
        {
            btnObject = GameObject.Find("backBtn");
            btn = btnObject.GetComponent<Button>();
            btn.onClick.AddListener(delegate { this.gotoMenu(); });
        }
        void gotoMenu()
        {
            SceneManager.LoadScene("loader");
            manager.switchState(new loaderState(manager, globalVar, "menuState"));
        }
        public void stateUpdate()
        {

        }
        public string getStateName()
        {
            return this.stateName;
        }
        public entityState GetEntity()
        {
            return entity;
        }
    }

}
