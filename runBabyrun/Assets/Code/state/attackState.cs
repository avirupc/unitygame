using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets.Code.interfaces;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Assets.Code.entity;
using Assets.Code.globalVar;

namespace Assets.Code.state
{
    public class attackState : iState
    {
        private stateManager manager;
        public string stateName;
        public groundEntity entity;

        public GameObject entityObject;
        public heroEntity1 hero;

        private GameObject menuButtonObject;
        private Button menuButton;

        private GameObject heroGameObject;
        private float startPositionX;

        private GameObject modal;
        private GameObject modalMenuButtonObject; 
        private Button  modalMenuButton;
        private GameObject modalresumeButtonObject;
        private Button modalresumeButton;
        private float delay = 0;
        private float heroLifeDelay = 0;

        public global globalVar;

        public attackState(stateManager managerRef,global globalRef)
        {
            manager = managerRef;
            globalVar = globalRef;
            stateName = "attackState";   


            globalVar.currentIndex = 0;
            globalVar.bulletlimit = 30;  
            globalVar.currentBulletIndex = globalVar.bulletlimit;
            globalVar.isReloading= false;
            globalVar.loaded = false;
            globalVar.currentAlpha = 0;
            globalVar.instantiateEnemy = true;
            globalVar.heroIsHitting = false;
            globalVar.isPause = false;
            globalVar.enemyRemain = globalVar.totalIncomingEnemy;
            globalVar.heroLife = 100f;
            globalVar.totalMagaZine = 10;
            globalVar.bornEnemy.RemoveAll(deleteAll);
            globalVar.bulletGraphicsPosX = 0;
            // globalVar.setActiveRound(globalVar.activeRound);

            Debug.Log("TOTAl INCOMING ENEMY ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
            Debug.Log(globalVar.bornEnemy.Count);

            Time.timeScale = 1;        
        }

        void OnEnable()
        {

        }

         private static bool deleteAll(string i) 
        { 
            return (i != null);
        } 

        void pause(){
            Time.timeScale = 0;
            globalVar.isPause = true;            
            modal.SetActive(true);
        }

        void resume(){
            Time.timeScale = 1;
            globalVar.isPause = false;            
            modal.SetActive(false);
        }
        void gotoMenu()
        {
            SceneManager.LoadScene("loader");
            manager.switchState(new loaderState(manager, globalVar, "menuState"));
        }


        public void getData()
        {
            modal = FindInActiveObjectByName("pauseModal");          
            modalMenuButtonObject = FindInActiveObjectByName("pauseModalMenu");
            modalMenuButton = modalMenuButtonObject.GetComponent<Button>();
            modalresumeButtonObject  = FindInActiveObjectByName("pauseModalResume");
            modalresumeButton = modalresumeButtonObject.GetComponent<Button>();
            menuButtonObject = GameObject.Find("menuButton");            
            menuButton = menuButtonObject.GetComponent<Button>();
            menuButton.onClick.AddListener(this.pause);

            modalMenuButton.onClick.AddListener(this.gotoMenu);
            modalresumeButton.onClick.AddListener(this.resume);

            
            // GET HERO ENTITY
            entityObject = GameObject.Find("entity");
            hero = entityObject.GetComponent<heroEntity1>();
            hero.globalVar = globalVar;
          
        }

        GameObject FindInActiveObjectByName(string name)
        {
            Transform[] objs = Resources.FindObjectsOfTypeAll<Transform>() as Transform[];
            for (int i = 0; i < objs.Length; i++)
            {
                if (objs[i].hideFlags == HideFlags.None)
                {
                    if (objs[i].name == name)
                    {
                        return objs[i].gameObject;
                    }
                }
            }
            return null;
        }
        public void stateUpdate()
        {
            if(globalVar.enemyRemain == 0 && globalVar.isWon && globalVar.bornEnemy.Count == 0){                
                delay += Time.deltaTime;
                if(delay*10 > 30){
                    wonTheRound();
                }
            }
            if(globalVar.heroLife <= 0){   
            Time.timeScale = 0;    
             lostRound();         
                // heroLifeDelay += Time.deltaTime;
                // if(heroLifeDelay*10 > 20){
                //     lostRound();
                // }
            }

        }
         void wonTheRound(){
         globalVar.isWon = false;
         SceneManager.LoadScene("wonState");
         manager.switchState(new wonState(manager,globalVar));
         // yield return null;
         }

         void lostRound(){
         SceneManager.LoadScene("lostState");
         manager.switchState(new lostState(manager,globalVar));
         // yield return null;
         }
       
        public void switchState()
        {
            SceneManager.LoadScene("loader");
            manager.switchState(new loaderState(manager,globalVar,"menuState"));
            
            // SceneManager.LoadScene("menuState");
            // manager.switchState(new menuState(manager,globalVar));
        }
        public string getStateName()
        {
            return this.stateName;
        }
        public entityState GetEntity()
        {
            return hero;
        }
    }
}
