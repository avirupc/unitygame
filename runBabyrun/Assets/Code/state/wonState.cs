﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets.Code.interfaces;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Assets.Code.entity;
using Assets.Code.globalVar;

namespace Assets.Code.state
{


    public class wonState : iState
    {
    	private stateManager manager;
    	public string stateName;
    	public groundEntity entity;

    	public global globalVar;

    	private GameObject playButtonObject;
    	private Button playButton;

    	private GameObject nextButtonObject;
    	private Button nextButton;

    	private GameObject menuButtonObject;
    	private Button menuButton;

    	public wonState(stateManager managerRef,global globalRef)
        {
        	manager = managerRef;
            globalVar = globalRef;
            stateName = "wonState"; 
        }

         public void getData(){

         	playButtonObject = GameObject.Find("playAgain");
         	playButton = playButtonObject.GetComponent<Button>();

         	//nextButtonObject = GameObject.Find("nextRound");
         	//nextButton = nextButtonObject.GetComponent<Button>();

         	menuButtonObject = GameObject.Find("menuButton");
         	menuButton = menuButtonObject.GetComponent<Button>();

         	playButton.onClick.AddListener(this.playAgain);
         	//nextButton.onClick.AddListener(this.nextRound);
         	menuButton.onClick.AddListener(this.goTomenu);

         }

         void playAgain(){         	
         	SceneManager.LoadScene("attackState");
            manager.switchState(new attackState(manager,globalVar));
         }

         void nextRound(){
         	int hasRound = globalVar.round.IndexOf(globalVar.activeRound);
         	if(hasRound != -1){
         		if(globalVar.round.IndexOf(globalVar.round[hasRound+1]) != -1)
         		globalVar.setActiveRound(globalVar.round[hasRound+1]);
         	}         	
         	SceneManager.LoadScene("attackState");
            manager.switchState(new attackState(manager,globalVar));
         }

         void goTomenu(){
         	// SceneManager.LoadScene("menuState");
            // manager.switchState(new menuState(manager,globalVar));

            SceneManager.LoadScene("loader");
            manager.switchState(new loaderState(manager,globalVar,"menuState"));
         }

         public void stateUpdate()
        {
        }
        public string getStateName()
        {
            return this.stateName;
        }
        public entityState GetEntity()
        {
            return entity;
        }

    }

}
