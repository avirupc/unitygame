using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Assets.Code.entity;
using Assets.Code.interfaces;
using Assets.Code.globalVar;

namespace Assets.Code.state
{
    public class menuState : iState
    {
        private stateManager manager;
        private GameObject buttonObject;
        private Button playButton;

        private GameObject exitButtonObject;
        private Button exitButton;

        private GameObject creditsButtonObject;
        private Button creditsButton;

        public global globalVar;

        private GameObject attackButtonObject;
        private Button attackButton;

        public string stateName;
        public entityState entity;

        // Modal Variable
        private GameObject modal;
        private GameObject modalYesButtonObject;
        private Button modalYesButton;
        private GameObject modalNoButtonObject;
        private Button modalNoButton;


        public menuState(stateManager managerRef,global globalRef)
        {
            Debug.Log("Menu Hogaya");
            manager = managerRef;
            globalVar = globalRef;
            stateName = "menuState";
            manager.speedlevel = 0;
            manager.increasing = false;
            
        }
        void credits()
        {
            SceneManager.LoadScene("loader");
            manager.switchState(new loaderState(manager, globalVar, "credits"));
        }
        void play()
        {
            this.switchState();
        }
        void exit()
        {
            Application.Quit();
        }

        void attack(){
            // AsyncOperation asyncOperation =  SceneManager.LoadScene("swampattack");
            // asyncOperation.allowSceneActivation = false;
            // Debug.Log("Pro :" + asyncOperation.progress);
            manager.switchState(new attackState(manager,globalVar));
        }
        public void getData()
        {


            modal = FindInActiveObjectByName("exitModal");
            modalYesButtonObject = FindInActiveObjectByName("yes");
            modalYesButton = modalYesButtonObject.GetComponent<Button>();
            modalNoButtonObject = FindInActiveObjectByName("no");
            modalNoButton = modalNoButtonObject.GetComponent<Button>();


            modalYesButton.onClick.AddListener(this.exit);
            modalNoButton.onClick.AddListener(this.back);

            buttonObject = GameObject.Find("Button");
            playButton = buttonObject.GetComponent<Button>();
            playButton.onClick.AddListener(this.play);

            exitButtonObject = GameObject.Find("exit");
            exitButton = exitButtonObject.GetComponent<Button>();
            exitButton.onClick.AddListener(this.openExitModal);

            attackButtonObject = GameObject.Find("attack");            
            attackButton = attackButtonObject.GetComponent<Button>();
            attackButton.onClick.AddListener(this.switchState);

            creditsButtonObject = GameObject.Find("credits");
            creditsButton = creditsButtonObject.GetComponent<Button>();
            creditsButton.onClick.AddListener(this.credits);

        }

        void back()
        {
            modal.SetActive(false);
        }

        void openExitModal()
        {   Debug.Log(modal);
            modal.SetActive(true);
        }

        GameObject FindInActiveObjectByName(string name)
        {
            Transform[] objs = Resources.FindObjectsOfTypeAll<Transform>() as Transform[];
            for (int i = 0; i < objs.Length; i++)
            {
                if (objs[i].hideFlags == HideFlags.None)
                {
                    if (objs[i].name == name)
                    {
                        return objs[i].gameObject;
                    }
                }
            }
            return null;
        }
        public void stateUpdate()
        {
            if (Input.GetKeyUp(KeyCode.A))
            {
                this.switchState();
            }
        }
        public void switchState()
        {
            // SceneManager.LoadScene("sceneSelectionState");
            // manager.switchState(new sceneSelectionState(manager,globalVar));
            SceneManager.LoadScene("loader");
            manager.switchState(new loaderState(manager,globalVar,"sceneSelectionState"));

        }
        public string getStateName()
        {
            return this.stateName;
        }
        public entityState GetEntity()
        {
            return entity;
        }
    }
}
