﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets.Code.interfaces;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Assets.Code.entity;
using Assets.Code.globalVar;

namespace Assets.Code.state
{
    public class playState : iState
    {
        private stateManager manager;
        private GameObject buttonObject;
        private Button playButton;
        public groundEntity entity;

        private Camera camera;
        private GameObject cameraObject;

        public string stateName;
        private float speedLimit;

        private GameObject sliderObject;
        private Slider slider;

        public global globalVar;

        public playState(stateManager managerRef, groundEntity entityRef, global globalRef)
        {
            manager = managerRef;
            stateName = "playState";
            globalVar = globalRef;

            speedLimit = 0;

            entity = entityRef;
            Debug.Log(entity);

            if (entity != null)
            {
                // entity.stopGround();
                entity.addForce(manager.speedlevel);
            }
        }

        void OnEnable()
        {

        }
        void gotoMenu()
        {
            this.switchState();
        }
        public void getData()
        {
            buttonObject = GameObject.Find("menuButton");
            playButton = buttonObject.GetComponent<Button>();
            Debug.Log(playButton);
            playButton.onClick.AddListener(this.gotoMenu);

            cameraObject = GameObject.Find("Main Camera");
            camera = cameraObject.GetComponent<Camera>();

            sliderObject = GameObject.Find("speedLimit");
            slider = sliderObject.GetComponent<Slider>();

            RaycastHit hit;
            Ray ray = camera.ScreenPointToRay(Input.mousePosition);
            entity = new groundEntity();
            // entity.addForce();
        }
        public void stateUpdate()
        {
            if (Input.GetKeyUp(KeyCode.A))
            {
                this.switchState();
            }
            if (manager.speedlevel != 0)
            {
                // Debug.Log(manager.speedlevel);
                // entity.addForce(manager.speedlevel);

            }

            if (manager.increasing && manager.speedlevel < 150)
            {
                manager.speedlevel += Time.deltaTime * 15;
                // Debug.Log(speedlevel);
                entity.addForce(manager.speedlevel);
            }
            else if (!manager.increasing && manager.speedlevel > 0)
            {
                manager.speedlevel -= Time.deltaTime * 15;
                entity.addForce(manager.speedlevel);
                // Debug.Log(speedlevel);
            }
            else if (manager.speedlevel > 148)
            {
                manager.increasing = false;
            }else if(manager.speedlevel < 0){
                manager.speedlevel = 0;
            }

            speedLimit = manager.speedlevel;
            if (slider != null)
                slider.value = speedLimit;


        }
        public void switchState()
        {
            SceneManager.LoadScene("menuState");
            manager.switchState(new menuState(manager,globalVar));
        }
        public string getStateName()
        {
            return this.stateName;
        }
        public entityState GetEntity()
        {
            return entity;
        }
    }
}
