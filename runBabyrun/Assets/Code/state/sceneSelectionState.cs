﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets.Code.interfaces;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Assets.Code.entity;
using Assets.Code.globalGamedata;
using Assets.Code.globalVar;

namespace Assets.Code.state
{
    public class sceneSelectionState : iState
    {
        private stateManager manager;
        public string stateName = "stateSelection";
        public groundEntity entity;
        public globalGameData gameData;

        public GameObject roundObject;
        public GameObject[] round;
        public GameObject scrollPanel;
        public RectTransform rect;
        public RectTransform scrollRect;
        public float buttonWidth;

        public GameObject comingSoonObject;

        public Text text;
        public GameObject textObject;
        public roundButton roundButtonCS;
        public global globalVar;

        public Button btn;
        public sceneSelectionState(stateManager managerRef, global globalRef)
        {
            manager = managerRef;
            globalVar = globalRef;
            Debug.Log("NEW GAMEDATA =================================================================================");
            //gameData = new globalGameData();
        }
        public void OnEnable()
        {

        }

        public void stateUpdate()
        {

        }
        public void getData()
        {
            scrollPanel = GameObject.Find("scrollPanel");
            scrollRect = scrollPanel.GetComponent<RectTransform>();
            round = new GameObject[globalVar.round.Count];

            for (int i = 0; i < globalVar.round.Count; i++)
            {
               if(i == 0)
                {
                    roundObject = Resources.Load("groundPrefab/round") as GameObject;
                    roundButtonCS = roundObject.GetComponent<roundButton>();
                    roundButtonCS.name = globalVar.round[i];

                    round[i] = GameObject.Instantiate(roundObject) as GameObject;
                    btn = round[i].GetComponent<Button>();
                    SetButtonOnClick(btn, globalVar.round[i]);
                    round[i].transform.parent = scrollPanel.transform;
                    round[i].transform.localScale = new Vector2(1, 1);
                    rect = round[i].GetComponent<RectTransform>();
                    buttonWidth = rect.rect.width;
                }
                else
                {
                    roundObject = Resources.Load("groundPrefab/comingSoon") as GameObject;
                    roundButtonCS = roundObject.GetComponent<roundButton>();
                    roundButtonCS.name = globalVar.round[i];

                    round[i] = GameObject.Instantiate(roundObject) as GameObject;
                    btn = round[i].GetComponent<Button>();
                    //SetButtonOnClick(btn, globalVar.round[i]);
                    round[i].transform.parent = scrollPanel.transform;
                    round[i].transform.localScale = new Vector2(1, 1);
                    rect = round[i].GetComponent<RectTransform>();
                    buttonWidth = rect.rect.width;
                }
                
                if (i != 0)
                {
                    rect.anchoredPosition = new Vector3((buttonWidth + 10) * i, 0,0);
                    rect.localPosition = new Vector3((buttonWidth + 10) * i, 0,0);
                }
                else
                {
                    rect.anchoredPosition = new Vector3(buttonWidth * i, 0,0);
                    rect.localPosition = new Vector3((buttonWidth + 10) * i, 0,0);
                }     
                if(i > 0)
                {
                    break;
                }
            }
            //GameObject = Resources.Load("groundPrefab/round") as GameObject;
        }
        void gotoPlay(string sceneName)
        {
            globalVar.setActiveRound(sceneName);
            // SceneManager.LoadScene("swampattack");
            // manager.switchState(new swampAttackState(manager,globalVar));

            SceneManager.LoadScene("loader");
            manager.switchState(new loaderState(manager,globalVar, "attackState"));
        }
        void SetButtonOnClick(Button btn, string name)
        {
            btn.onClick.AddListener(delegate { this.gotoPlay(name); });

        }
        public string getStateName()
        {
            return this.stateName;
        }
        public entityState GetEntity()
        {
            return entity;
        }

    }
}
