﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets.Code.interfaces;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Assets.Code.entity;
namespace Assets.Code.state
{

    public class moveState : iState
    {
        private stateManager manager;
        public string stateName;
        public groundEntity entity;
        public moveState(stateManager managerRef, groundEntity entityRef)
        {
            manager = managerRef;
            // Debug.Log(manager.gameObject);
            // Debug.Log(manager.getActiveState());
            stateName = "moveState";
            entity = entityRef;
            // entity.addForce();
        }
        public void getData() { }
        public void stateUpdate() { }
        public string getStateName()
        {
            return this.stateName;
        }
        public entityState GetEntity()
        {
            return entity;
        }
    }
}

