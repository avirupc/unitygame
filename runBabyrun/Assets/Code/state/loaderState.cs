﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets.Code.interfaces;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Assets.Code.entity;
using Assets.Code.globalVar;

namespace Assets.Code.state
{


    public class loaderState : iState
    {
    	private stateManager manager;
    	public string stateName;
    	public loaderEntity entity;
    	public iState nextstate;
    	public string scene;

    	private bool goNextState = false;

    	GameObject entityObject;

    	public global globalVar;


    	public loaderState(stateManager managerRef,global globalRef, string sceneName)
        {
        	manager = managerRef;
            globalVar = globalRef;
            // nextstate = nextState;


            scene = sceneName;
            stateName = "loaderState"; 
        }

         public void getData(){
         	// GET HERO ENTITY
            entityObject = GameObject.Find("entity");
            entity = entityObject.GetComponent<loaderEntity>();
            entity.sceneName = scene;
            entity.globalVar = globalVar;
            entity.manager = manager;
            entity.LoadButton();         	       	
         }
     
         

        public void stateUpdate()
        {
        }
        public string getStateName()
        {
            return this.stateName;
        }
        public entityState GetEntity()
        {
            return entity;
        }

    }

}


