﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class panelController : MonoBehaviour
{
	public GameObject mainPanel;
	public RectTransform panel; // to hold the scroll panel
	public GameObject[] btnPrefab;
	public Button[] btn;
	public GameObject centerObject;
	public RectTransform center; // Center to compare the distance the each button

	private float[] distance; // All button distance to the center
	private bool dragging = false;
	private int btnDistance; // will hold distance between button
	private int minBtnNum; // nearest button of h center

    // Start is called before the first frame update
    void Start()
    {
    	mainPanel = GameObject.Find("scrollPanel");
    	panel = mainPanel.GetComponent<RectTransform>();

    	centerObject = GameObject.Find("center");
    	center = centerObject.GetComponent<RectTransform>();
    	btnPrefab = GameObject.FindGameObjectsWithTag("roundButton");
    	btn = new Button[btnPrefab.Length];
    	Debug.Log(btnPrefab.Length);
    	for(int x=0 ; x < btnPrefab.Length; x++){    		
    		btn[x] = btnPrefab[x].GetComponent<Button>();
    	}

        int btnLength = btn.Length;
        distance = new float[btnLength];
        // Get distance between button
        btnDistance = (int)Mathf.Abs(btn[1].GetComponent<RectTransform>().anchoredPosition.x-btn[0].GetComponent<RectTransform>().anchoredPosition.x);        
        Debug.Log(btnDistance);
        Debug.Log(centerObject);
        Debug.Log(panel);
    }

    // Update is called once per frame
    void Update()
    {    	
        for(int x=0 ; x < btn.Length; x++){
        	distance[x] = Mathf.Abs(center.transform.position.x-btn[x].transform.position.x);
        }

        float minDistance = Mathf.Min(distance);

        for(int a =0 ; a < btn.Length; a++){
        	if(minDistance == distance[a]){
        		minBtnNum = a;
        	}        	
        }
        if(!dragging){        	
        	stuckBtn(minBtnNum*-btnDistance);
        }
    }
    void stuckBtn(int position){
    	float newX = Mathf.Lerp(panel.anchoredPosition.x,position,Time.deltaTime *10f);
    	Vector2 newPosition = new Vector2(newX, panel.anchoredPosition.y);
    	panel.anchoredPosition = newPosition;
    	Debug.Log(newPosition);
    }

    public void startDrag(){
    	dragging = true;
    }
    public void endDrag(){
    	dragging = false;
    }
}
