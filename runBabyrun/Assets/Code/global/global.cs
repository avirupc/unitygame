﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Code.globalVar {
	public class global
{

	public int currentIndex = 0;
	public int bulletlimit = 30;

	public int totalBullet;

	public int currentBulletIndex;

	public bool isReloading = false;
	public bool loaded = false;

	public float currentAlpha = 0;

	public bool instantiateEnemy = true;

	public bool heroIsHitting = false;

	public List<string> bornEnemy = new List<string>() ;

	public string activeRound;

	public bool isPause = false;

	public int totalIncomingEnemy = 15;
	public int enemyRemain;

	public bool isWon = false;

	public List<string> round = new List<string>(); 

	public int totalMagaZine;

	public string firstEnemy;

	public float heroLife = 100f;
	public bool isFiring = false;


	public Vector2 lastMousePosition;

    public float bulletGraphicsPosX = 0f;
    public global(){
    	currentBulletIndex = bulletlimit;
    	enemyRemain = totalIncomingEnemy;
    	round.Add("round1");
    	round.Add("round2");
    	round.Add("round3");
    	round.Add("round4");

    	totalMagaZine = 10;
    	totalBullet = bulletlimit*totalMagaZine;
    }

    public void setActiveRound(string round){
    	this.activeRound = round;
    	if(round == "round1"){
            setTotalIncomingEnemy(8);
        }else if(round == "round2"){
            setTotalIncomingEnemy(30);
         }
    }

    public void setTotalIncomingEnemy(int enemyNum){
    	this.totalIncomingEnemy = enemyNum;
    }

    public void reload(){
    isReloading = true;
    currentBulletIndex = currentBulletIndex+1;	
    } 
}

}

