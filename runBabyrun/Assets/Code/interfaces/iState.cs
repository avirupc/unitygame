﻿using Assets.Code.entity;

namespace Assets.Code.interfaces
{
    public interface iState
    {
        void stateUpdate();
        void getData();
        string getStateName();
        entityState GetEntity();

    }
}