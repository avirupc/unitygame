﻿
using UnityEngine;

namespace Assets.Code.interfaces
{
    public interface entityState
    {
    	void fire(Vector2 dest);
    	void stopFire();
    }
}