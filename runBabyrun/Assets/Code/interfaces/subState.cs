﻿using UnityEngine;

namespace Assets.Code.interfaces
{
    public interface subState
    {
    	void getData();
    }
}