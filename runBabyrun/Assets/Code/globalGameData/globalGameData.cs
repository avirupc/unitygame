﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Code.globalGamedata
{

    public class globalGameData
    {
        public string[] scenes = new string[3];
        public string activeScene;

        public globalGameData()
        {
            scenes[0] = "normal_hill";
            scenes[1] = "normal_desert";
            scenes[2] = "normal_swamp";

        }

        public void setActive(string scene)
        {
            for (int i = 0; i < this.scenes.Length; i++)
            {
                if (scenes[i] == scene)
                {
                    this.activeScene = scenes[i];
                    break;
                }
            }
        }

    }
}




